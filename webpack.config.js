const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');

module.exports = {
    // entry : definir pour webpack l'emplecement de demarrer le bundle
    entry: path.join(__dirname, "src", "index.js"),
    // output : definir pour webpack l'emplacement pour le bundle finale dist dans le root
    output: {
        path: path.join(__dirname, 'dist')
    },
    plugins: [
        // definir le fichier html pour que webpack injecte le bundle js generer
        new HtmlWebpackPlugin({
            template: path.join(__dirname, "src", "index.html")
        })
    ]
}